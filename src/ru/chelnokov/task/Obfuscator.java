package ru.chelnokov.task;

import java.io.*;
import java.util.ArrayList;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Класс, получающий и обфусцирующий код
 *
 * @author Chelnokov E.I., 16IT18K
 * @author Shishkova D.R.., 16IT18K
 */
public class Obfuscator {
    public static void main(String[] args) {
        String filePath = "src//ru//chelnokov//code//MultiTask.java";
        File file = new File(filePath);
        ArrayList<String> contents = readTheData(filePath);

        contents = deleteComments(contents);//удаляет комментарии
        StringBuilder sb = deleteSpaces(contents);//удаляет лишние пробелы

        String className = file.getName();
        writeClass(sb, className);//записывает в файл, меняя имя класса и конструкторов
    }

    /**
     * Возвращает ArrayList с построчно считанными данными
     * @param filePath путь к считываемому файлу
     * @return ArrayList со считанными данными
     */
    private static ArrayList<String> readTheData(String filePath) {
        ArrayList<String> contents = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader(filePath))) {
            String string;
            while ((string = bufferedReader.readLine()) != null) {
                contents.add(string);
            }
        } catch (IOException e) {
            System.out.println("Извините, при работе с кодом произошла ошибка");
        }
        return contents;
    }

    /**
     * Возвращает ArrayList с удалёнными лишними пробелами
     * Лишними пробелами считаются те, что:
     * повторяются два и более раз;
     * находятся рядом с символами, не удовлетворяющими условиям идентификатора;
     *
     * @author Chelnokov E.I
     * @param strings исходный ArrayList со строками данных
     * @return ArrayList, очищенный от лишних пробелов
     */
    private static StringBuilder deleteSpaces(ArrayList<String> strings) {
        StringBuilder sb = new StringBuilder();
        Pattern pattern = Pattern.compile("( \\W{1,3})|( \\W{1,3} )|(\\W{1,3} )");
        for (String string : strings) {
            string = string.replaceAll("\\s{2,}", "");
            Matcher matcher = pattern.matcher(string);
            while (matcher.find()) {
                string = string.replace(matcher.group(), matcher.group().trim());
            }
            sb.append(string);
        }
        return sb;
    }

    /**
     * Возвращает ArrayList кода, очищенного от комментариев
     * Метод работает на всех видов комментариев
     * @author Shiskova D.R., 16IT18K
     * @param strings исходный ArrayList, содержащий комментарии
     * @return ArrayList без комментариев
     */
    private static ArrayList<String> deleteComments(ArrayList<String> strings) {
        String temp;
        ArrayList<String> withoutComments = new ArrayList<>();
        boolean isComment = false;
        for (int i = 0; i < strings.size(); i++) {
            temp = strings.get(i);
            if (!isComment) {
                for (int j = 0; j < temp.length() - 1; j++) {
                    if (temp.charAt(j) == '/' && temp.charAt(j + 1) == '*') {
                        isComment = true;
                    }
                }
            }
            if (isComment) {
                for (int j = 0; j < temp.length() - 1; j++) {
                    if (temp.charAt(j) == '*' && temp.charAt(j + 1) == '/') {
                        isComment = false;
                        i++;
                    }
                }
            }
            if (!isComment) {
                if (strings.get(i).contains("//")) {
                    String[] stringParts = strings.get(i).split("//");
                    withoutComments.add(stringParts[0]);
                } else {
                    withoutComments.add(strings.get(i));
                }
            }
        }
        return withoutComments;
    }

    /**
     * Возвращает случайную букву английского алфавита
     * @return случайный символ алфавита
     */
    private static char getRandomLetter() {
        Random random = new Random();
        String alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

        return alphabet.charAt(random.nextInt(alphabet.length()));
    }

    /**
     * Записывает класс в файл, изменяя имя класса и его конструкторов
     * @param contents объект StringBuilder, исходный код одной строкой
     * @param oldName текущее имя класса
     */
    private static void writeClass(StringBuilder contents, String oldName) {
        oldName = oldName.replace(".java", "");
        String newName = String.valueOf(getRandomLetter());
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("src//ru//chelnokov//code//" + newName + ".java"))) {
            bufferedWriter.write(String.valueOf(contents).replaceAll(oldName, newName));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}